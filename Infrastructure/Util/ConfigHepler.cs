﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;
namespace Infrastructure
{
    public class ConfigHepler
    {
        private IConfigurationRoot _config { get; set; }
        private static object syncRoot = new object();
        private static ConfigHepler _instance;


        private ConfigHepler()
        {
            _config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                                   .AddJsonFile("appsettings.json")
                                                   .Build();
        }

        private static ConfigHepler GetInstance()
        {
            if (_instance == null)
            {
                lock (syncRoot)
                {
                    if (_instance == null)
                    {
                        return new ConfigHepler();
                    }
                }
            }
            return _instance;
        }

        public static string GetSection(string key)
        {
            return GetInstance()._config.GetSection(key).Value;
        }
    }
}
