﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Entity;
using System.Collections.Generic;

namespace Infrastructure
{
    public static class ServiceConatinerExtensions
    {
        private static IContainer _container;
        private static ContainerBuilder builder;

        public static void SetContainer(this IServiceCollection services)
        {
            _container = BuildContainer(services);
        }
        public static IServiceProvider InjectAutofacServiceProvider(this IServiceCollection services)
        {
            return new AutofacServiceProvider(_container);
        }

        public static IContainer GetContainer()
        {
            return _container;
        }

        private static IContainer BuildContainer(IServiceCollection services)
        {
            builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterAssemblyTypes(Assembly.Load("api")).AsImplementedInterfaces();
            builder.AddRegistriesForRepository();
            return builder.Build();
        }

        private static ContainerBuilder AddRegistriesForRepository(this ContainerBuilder builder)
        {

            Assembly.Load("Repository.interfaces");
            var types = Assembly.Load("Repository.DefaultImplementation");

            foreach (var @type in types.GetTypes().Where(x => !x.IsAbstract && x.Name.EndsWith("Repository")))
            {
                var @interface = @type.GetInterface($"I{@type.Name}");
                builder.RegisterType(@type).As(@interface);
            }
            return builder;
        }

        public static void ScanTypes(IEnumerable<RegisterTypeEntity> register)
        {
            if (register == null || register.Any()) return;
            foreach (var item in register)
            {
                builder.RegisterType(item.Type).As(item.Interface);
            }
        }
    }
}
