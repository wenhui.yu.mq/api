﻿using Autofac;

namespace Infrastructure
{
    public static class ServiceFactory
    {
        private static readonly IContainer _container;
        static ServiceFactory()
        {
            _container = ServiceConatinerExtensions.GetContainer();
        }
        public static T Get<T>()
        {
            T instance;
            using (var scope = _container.BeginLifetimeScope())
            {
                scope.TryResolve<T>(out instance);
            }
            return instance;
        }
    }
}
