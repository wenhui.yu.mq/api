﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using sdmap.ext.Dapper;

namespace Repository.DefaultImplementation
{
    public static class DbUtil
    {
        public async static Task<T> FirstOrDefault<T>(string sqlFullName, object param)
        {
            using (var db = new MySqlDBConnectionProvider().DbConnection)
            {
                Trace.WriteLine($"sql:------ {DbConnectionExtensions.EmitSql(sqlFullName, param)}");
                return await db.QueryFirstByMapAsync<T>(sqlFullName, param);
            }
        }
    }
}
