using Repository.interfaces;
using System;
using System.Threading.Tasks;
using sdmap.ext.Dapper;
using System.Runtime.CompilerServices;
using DapperExtensions;
using System.Data;
using Infrastructure;

namespace Repository.DefaultImplementation
{
    public abstract class SdmapRepositoryBase : IRepositoryBase
    {
        protected virtual string SqlNamespace => GetType().Name;

        private readonly IDbConnectionProvider _dbConnectionProvider;

        static SdmapRepositoryBase()
        {
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
            DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
            DbConnectionExtensions.SetEmbeddedSqlAssembly(typeof(SdmapRepositoryBase).Assembly);
        }
        protected SdmapRepositoryBase()
        {
            _dbConnectionProvider = ServiceFactory.Get<IDbConnectionProvider>();
        }

        protected async virtual Task<T> FirstOrDefault<T>(object paramer, [CallerMemberName]string sqlFunction = "")
        {
            return await DbUtil.FirstOrDefault<T>(GetSqlFullname(sqlFunction), paramer);
        }

        private string GetSqlFullname(string sqlFunc)
        {
            return $"{SqlNamespace}.{sqlFunc}";
        }

        public async virtual Task<T> GetAsync<T, K>(K primary) where T : class
        {
            using (IDbConnection conn = _dbConnectionProvider.DbConnection)
            {
                return await Task.FromResult(conn.Get<T>(primary));
            }
        }

        public T Get<T, K>(K primary)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetListAsync<T>(Func<T, bool> func)
        {
            throw new NotImplementedException();
        }

        public T GetList<T>(Func<T, bool> func)
        {
            throw new NotImplementedException();
        }

        public Task<T> CreateAsync<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public T Create<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync<T>(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
