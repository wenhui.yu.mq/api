﻿using Entity;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;

namespace Repository.DefaultImplementation
{
    public class DBContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(ConfigHepler.GetSection(nameof(AppsettingModel.DefaultConnectionString)));
        }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Product> Product { get; set; }
    }
}
