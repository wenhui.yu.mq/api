﻿using System.Data;

using Entity;
using Repository.interfaces;
using MySql.Data.MySqlClient;
using Infrastructure;

namespace Repository.DefaultImplementation
{
    public sealed class MySqlDBConnectionProvider : IDbConnectionProvider
    {

        public IDbConnection DbConnection => _dbConn;
        private readonly static IDbConnection _dbConn;

        static MySqlDBConnectionProvider()
        {
            _dbConn =  new MySqlConnection(ConfigHepler.GetSection(nameof(AppsettingModel.DefaultConnectionString)));
        }
    }
}
