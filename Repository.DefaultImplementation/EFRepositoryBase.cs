﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repository.interfaces;

namespace Repository.DefaultImplementation
{
    public abstract class EFRepositoryBase : IRepositoryBase
    {
        private static readonly DBContext _dBContext;

        static EFRepositoryBase()
        {
            _dBContext = new DBContext();
        }

        public T Create<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<T> CreateAsync<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public T Get<T, K>(K primary)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetAsync<T, K>(K primary) where T : class
        {
            throw new NotImplementedException();
        }

        public T GetList<T>(Func<T, bool> func)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetListAsync<T>(Func<T, bool> func)
        {
            throw new NotImplementedException();
        }
    }
}
