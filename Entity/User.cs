﻿using DapperExtensions.Mapper;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity
{
    [Table("nico_User")]
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
    }

    /// <summary>
    /// For  Dapper 
    /// </summary>
    public class UserMapper : ClassMapper<User>
    {
        public UserMapper()
        {
            base.Table("nico_User");
            Map(x => x.Address).Ignore();
            AutoMap();
        }
    }
}
