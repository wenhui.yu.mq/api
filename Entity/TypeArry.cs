﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Entity
{
    public class RegisterTypeEntity
    {
        public Type @Type { get; set; }
        public Type @Interface { get; set; }
    }
}
