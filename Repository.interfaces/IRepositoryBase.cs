﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.interfaces
{
    public interface IRepositoryBase
    {
        Task<T> GetAsync<T, K>(K primary) where T : class;
        T Get<T, K>(K primary);
        Task<T> GetListAsync<T>(Func<T, bool> func);
        T GetList<T>(Func<T, bool> func);

        Task<T> CreateAsync<T>(T entity);
        T Create<T>(T entity);

        bool Delete<T>(T entity);
        Task<bool> DeleteAsync<T>(T entity);
    }
}
