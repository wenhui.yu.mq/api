﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Repository.interfaces
{
    public interface IDbConnectionProvider
    {
        IDbConnection DbConnection { get; }
    }
}
