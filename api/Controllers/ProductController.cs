﻿using Microsoft.AspNetCore.Mvc;
using Repository.DefaultImplementation;
using Repository.interfaces;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Entity;
using Infrastructure;
using Autofac;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {

        private readonly IUserRepository _userRepository;

        public ProductController(IUserRepository userRepositoryt,
                                 ILogger<ProductController> logger)
        {
            _userRepository = userRepositoryt;
        }

        [HttpGet("List")]
        public async Task<IActionResult> List()
        {
            var repo = _userRepository as UserRepository;
            var result = await repo.GetAsync<User, string>("1");


            return new JsonResult(result);

        }
    }
}

